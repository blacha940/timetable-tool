from building_api.viewsets import BuildingViewset
from class_api.viewsets import ClassViewset
from classroom_api.viewsets import ClassroomViewset
from classroom_subject_api.viewsets import ClassroomSubjectViewset
from lesson_api.viewsets import LessonViewset
from preference_api.viewsets import PreferenceViewset
from preference_type_api.viewsets import PreferenceTypeViewset
from subject_api.viewsets import SubjectViewset
from teacher_api.viewsets import TeacherViewset
from teacher_subject_api.viewsets import TeacherSubjectViewset
from timebox_api.viewsets import TimeboxViewset
from timetable_api.viewsets import TimetableViewset
from weekday_api.viewsets import WeekdayViewset

from rest_framework import routers

router = routers.DefaultRouter()
router.register("buildnigs", BuildingViewset)
router.register("class", ClassViewset)
router.register("classroom", ClassroomViewset)
router.register("classroom_subject", ClassroomSubjectViewset)
router.register("lesson", LessonViewset)
router.register("preference", PreferenceViewset)
router.register("preference_type", PreferenceTypeViewset)
router.register("subjects", SubjectViewset)
router.register("teachers", TeacherViewset)
router.register("teacher_subject", TeacherSubjectViewset)
router.register("timebox", TimeboxViewset)
router.register("timetable", TimetableViewset)
router.register("weekday", WeekdayViewset)
