from django.apps import AppConfig


class BuildingApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'building_api'
