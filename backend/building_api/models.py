from django.db import models


class Building(models.Model):
    name = models.CharField(max_length=100)
    street = models.CharField(max_length=50, blank=True)
    house_number = models.CharField(max_length=6, blank=True)
    postal_code = models.CharField(max_length=6, blank=True)
    city = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return self.name + " " + self.street + " " + self.house_number + " " + self.postal_code + " " + self.city
