from rest_framework import viewsets
from . import models
from . import serializer


class BuildingViewset(viewsets.ModelViewSet):
    queryset = models.Building.objects.all()
    serializer_class = serializer.BuildingSerializer
