from rest_framework import viewsets
from . import models
from . import serializer


class ClassViewset(viewsets.ModelViewSet):
    queryset = models.Class.objects.all()
    serializer_class = serializer.ClassSerializer
