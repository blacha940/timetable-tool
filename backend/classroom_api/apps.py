from django.apps import AppConfig


class ClassroomApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'classroom_api'
