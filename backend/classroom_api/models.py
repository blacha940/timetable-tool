from django.db import models
from django.db.models.deletion import CASCADE
from building_api.models import Building


class Classroom(models.Model):
    name = models.CharField(max_length=15)
    capacity = models.IntegerField()
    building = models.ForeignKey(Building, on_delete=CASCADE)
    description = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.name
