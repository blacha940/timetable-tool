from rest_framework import viewsets
from . import models
from . import serializer


class ClassroomViewset(viewsets.ModelViewSet):
    queryset = models.Classroom.objects.all()
    serializer_class = serializer.ClassroomSerializer
