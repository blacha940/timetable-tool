from django.apps import AppConfig


class ClassroomSubjectApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'classroom_subject_api'
