from django.db import models
from django.db.models.deletion import CASCADE
from classroom_api.models import Classroom
from subject_api.models import Subject


class ClassroomSubject(models.Model):
    classroom = models.ForeignKey(Classroom, on_delete=CASCADE)
    subject = models.ForeignKey(Subject, on_delete=CASCADE)
