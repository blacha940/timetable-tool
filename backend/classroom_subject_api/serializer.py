from rest_framework import serializers
from .models import ClassroomSubject


class ClassroomSubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClassroomSubject
        fields = "__all__"
