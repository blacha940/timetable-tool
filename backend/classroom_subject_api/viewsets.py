from rest_framework import viewsets
from . import models
from . import serializer


class ClassroomSubjectViewset(viewsets.ModelViewSet):
    queryset = models.ClassroomSubject.objects.all()
    serializer_class = serializer.ClassroomSubjectSerializer
