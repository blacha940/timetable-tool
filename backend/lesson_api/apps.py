from django.apps import AppConfig


class LessonApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lesson_api'
