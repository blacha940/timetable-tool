from django.db.models.deletion import CASCADE
from django.db import models
from django.db import models
from django.db.models.fields.related import ForeignKey
from class_api.models import Class
from classroom_api.models import Classroom
from subject_api.models import Subject
from teacher_api.models import Teacher
from timebox_api.models import Timebox
from timetable_api.models import Timetable
from weekday_api.models import Weekday


class Lesson(models.Model):
    teacher = models.ForeignKey(Teacher, on_delete=CASCADE)
    subject = models.ForeignKey(Subject, on_delete=CASCADE)
    class_id = models.ForeignKey(Class, on_delete=CASCADE)
    timebox = models.ForeignKey(Timebox, on_delete=CASCADE)
    week_day = models.ForeignKey(Weekday, on_delete=CASCADE)
    classroom = models.ForeignKey(Classroom, on_delete=CASCADE)
    timetable = models.ForeignKey(Timetable, on_delete=CASCADE)

    def __str__(self):
        return self.teacher + " " + self.subject + " " + self.class_id + " " + self.timebox + " " + self.week_day\
            + " " + self.classroom
