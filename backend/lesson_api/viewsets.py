from rest_framework import viewsets
from . import models
from . import serializer


class LessonViewset(viewsets.ModelViewSet):
    queryset = models.Lesson.objects.all()
    serializer_class = serializer.LessonSerializer
