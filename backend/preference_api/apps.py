from django.apps import AppConfig


class PreferenceApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'preference_api'
