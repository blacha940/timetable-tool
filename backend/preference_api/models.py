from django.db.models.deletion import CASCADE
from django.db import models
from django.db import models
from preference_type_api.models import PreferenceType
from teacher_api.models import Teacher
from timebox_api.models import Timebox
from weekday_api.models import Weekday


class Preference(models.Model):
    teacher = models.ForeignKey(Teacher, on_delete=CASCADE)
    preference_type = models.ForeignKey(PreferenceType, on_delete=CASCADE)
    timebox = models.ForeignKey(Timebox, on_delete=CASCADE)
    week_day = models.ForeignKey(Weekday, on_delete=CASCADE)
    description = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.teacher + " " + self.preference_type + " " + str(self.timebox) + " " + self.week_day
