from rest_framework import viewsets
from . import models
from . import serializer


class PreferenceViewset(viewsets.ModelViewSet):
    queryset = models.Preference.objects.all()
    serializer_class = serializer.PreferenceSerializer
