from django.apps import AppConfig


class PreferenceTypeApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'preference_type_api'
