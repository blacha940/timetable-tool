from django.db import models
from django.db import models


class PreferenceType(models.Model):
    name = models.CharField(max_length=50)
    colour = models.CharField(max_length=15)

    def __str__(self):
        return self.name + " " + self.colour
