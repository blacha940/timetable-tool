from rest_framework import serializers
from .models import PreferenceType


class PreferenceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PreferenceType
        fields = "__all__"
