from rest_framework import viewsets
from . import models
from . import serializer


class PreferenceTypeViewset(viewsets.ModelViewSet):
    queryset = models.PreferenceType.objects.all()
    serializer_class = serializer.PreferenceTypeSerializer
