from rest_framework import viewsets
from . import models
from . import serializer


class SubjectViewset(viewsets.ModelViewSet):
    queryset = models.Subject.objects.all()
    serializer_class = serializer.SubjectSerializer
