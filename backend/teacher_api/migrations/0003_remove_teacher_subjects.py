# Generated by Django 3.2.3 on 2021-06-11 20:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teacher_api', '0002_rename_teachers_teacher'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='teacher',
            name='subjects',
        ),
    ]
