from rest_framework import viewsets
from . import models
from . import serializer


class TeacherViewset(viewsets.ModelViewSet):
    queryset = models.Teacher.objects.all()
    serializer_class = serializer.TeacherSerializer
