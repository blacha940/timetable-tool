from django.apps import AppConfig


class TeacherSubjectApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'teacher_subject_api'
