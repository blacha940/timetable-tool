from django.db import models
from django.db.models.deletion import CASCADE
from teacher_api.models import Teacher
from subject_api.models import Subject


class TeacherSubject(models.Model):
    teacher = models.ForeignKey(Teacher, on_delete=CASCADE)
    subject = models.ForeignKey(Subject, on_delete=CASCADE)
