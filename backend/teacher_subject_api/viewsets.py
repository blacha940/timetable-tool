from rest_framework import viewsets
from . import models
from . import serializer


class TeacherSubjectViewset(viewsets.ModelViewSet):
    queryset = models.TeacherSubject.objects.all()
    serializer_class = serializer.TeacherSubjectSerializer
