from django.apps import AppConfig


class TimeboxApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'timebox_api'
