from django.db import models


class Timebox(models.Model):
    start = models.TimeField()
    stop = models.TimeField()

    def __str__(self):
        return str(self.start) + " - " + str(self.stop)
