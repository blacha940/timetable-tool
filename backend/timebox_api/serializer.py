from rest_framework import serializers
from .models import Timebox


class TimeboxSerializer(serializers.ModelSerializer):
    class Meta:
        model = Timebox
        fields = "__all__"
