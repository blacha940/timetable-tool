from rest_framework import viewsets
from . import models
from . import serializer


class TimeboxViewset(viewsets.ModelViewSet):
    queryset = models.Timebox.objects.all()
    serializer_class = serializer.TimeboxSerializer
