from django.db import models


class Timetable(models.Model):
    name = models.CharField(max_length=255)
    creation_date = models.DateField()
    date_of_acceptance = models.DateField(blank=True, default=None)
    is_accepted = models.BooleanField(default=False)

    def __str__(self):
        return self.name
