from rest_framework import viewsets
from . import models
from . import serializer


class TimetableViewset(viewsets.ModelViewSet):
    queryset = models.Timetable.objects.all()
    serializer_class = serializer.TimetableSerializer
