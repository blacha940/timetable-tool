from django.apps import AppConfig


class WeekdayApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'weekday_api'
