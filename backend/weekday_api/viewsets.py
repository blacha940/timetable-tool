from rest_framework import viewsets
from . import models
from . import serializer


class WeekdayViewset(viewsets.ModelViewSet):
    queryset = models.Weekday.objects.all()
    serializer_class = serializer.WeekdaySerializer
