PGDMP                         y        	   school_db     13.3 (Ubuntu 13.3-1.pgdg20.04+1)     13.3 (Ubuntu 13.3-1.pgdg20.04+1) �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    24930 	   school_db    DATABASE     ^   CREATE DATABASE school_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';
    DROP DATABASE school_db;
                postgres    false            �            1259    24962 
   auth_group    TABLE     f   CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);
    DROP TABLE public.auth_group;
       public         heap    postgres    false            �            1259    24960    auth_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.auth_group_id_seq;
       public          postgres    false    207            �           0    0    auth_group_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;
          public          postgres    false    206            �            1259    24972    auth_group_permissions    TABLE     �   CREATE TABLE public.auth_group_permissions (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);
 *   DROP TABLE public.auth_group_permissions;
       public         heap    postgres    false            �            1259    24970    auth_group_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.auth_group_permissions_id_seq;
       public          postgres    false    209            �           0    0    auth_group_permissions_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;
          public          postgres    false    208            �            1259    24954    auth_permission    TABLE     �   CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);
 #   DROP TABLE public.auth_permission;
       public         heap    postgres    false            �            1259    24952    auth_permission_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.auth_permission_id_seq;
       public          postgres    false    205            �           0    0    auth_permission_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;
          public          postgres    false    204            �            1259    24980 	   auth_user    TABLE     �  CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);
    DROP TABLE public.auth_user;
       public         heap    postgres    false            �            1259    24990    auth_user_groups    TABLE     ~   CREATE TABLE public.auth_user_groups (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 $   DROP TABLE public.auth_user_groups;
       public         heap    postgres    false            �            1259    24988    auth_user_groups_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.auth_user_groups_id_seq;
       public          postgres    false    213            �           0    0    auth_user_groups_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;
          public          postgres    false    212            �            1259    24978    auth_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.auth_user_id_seq;
       public          postgres    false    211            �           0    0    auth_user_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;
          public          postgres    false    210            �            1259    24998    auth_user_user_permissions    TABLE     �   CREATE TABLE public.auth_user_user_permissions (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 .   DROP TABLE public.auth_user_user_permissions;
       public         heap    postgres    false            �            1259    24996 !   auth_user_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.auth_user_user_permissions_id_seq;
       public          postgres    false    215            �           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;
          public          postgres    false    214            �            1259    32798    building_api_building    TABLE     #  CREATE TABLE public.building_api_building (
    id bigint NOT NULL,
    name character varying(100) NOT NULL,
    street character varying(50) NOT NULL,
    house_number character varying(6) NOT NULL,
    postal_code character varying(6) NOT NULL,
    city character varying(50) NOT NULL
);
 )   DROP TABLE public.building_api_building;
       public         heap    postgres    false            �            1259    32796    building_api_building_id_seq    SEQUENCE     �   CREATE SEQUENCE public.building_api_building_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.building_api_building_id_seq;
       public          postgres    false    226            �           0    0    building_api_building_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.building_api_building_id_seq OWNED BY public.building_api_building.id;
          public          postgres    false    225            �            1259    32845    class_api_class    TABLE     �   CREATE TABLE public.class_api_class (
    id bigint NOT NULL,
    name character varying(50) NOT NULL,
    number_of_students integer NOT NULL
);
 #   DROP TABLE public.class_api_class;
       public         heap    postgres    false            �            1259    32843    class_api_class_id_seq    SEQUENCE        CREATE SEQUENCE public.class_api_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.class_api_class_id_seq;
       public          postgres    false    232            �           0    0    class_api_class_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.class_api_class_id_seq OWNED BY public.class_api_class.id;
          public          postgres    false    231            �            1259    32806    classroom_api_classroom    TABLE     �   CREATE TABLE public.classroom_api_classroom (
    id bigint NOT NULL,
    name character varying(15) NOT NULL,
    capacity integer NOT NULL,
    description character varying(255) NOT NULL,
    building_id bigint NOT NULL
);
 +   DROP TABLE public.classroom_api_classroom;
       public         heap    postgres    false            �            1259    32804    classroom_api_classroom_id_seq    SEQUENCE     �   CREATE SEQUENCE public.classroom_api_classroom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.classroom_api_classroom_id_seq;
       public          postgres    false    228            �           0    0    classroom_api_classroom_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.classroom_api_classroom_id_seq OWNED BY public.classroom_api_classroom.id;
          public          postgres    false    227            �            1259    32820 &   classroom_subject_api_classroomsubject    TABLE     �   CREATE TABLE public.classroom_subject_api_classroomsubject (
    id bigint NOT NULL,
    subject_id bigint NOT NULL,
    classroom_id bigint NOT NULL
);
 :   DROP TABLE public.classroom_subject_api_classroomsubject;
       public         heap    postgres    false            �            1259    32818 -   classroom_subject_api_classroomsubject_id_seq    SEQUENCE     �   CREATE SEQUENCE public.classroom_subject_api_classroomsubject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.classroom_subject_api_classroomsubject_id_seq;
       public          postgres    false    230            �           0    0 -   classroom_subject_api_classroomsubject_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.classroom_subject_api_classroomsubject_id_seq OWNED BY public.classroom_subject_api_classroomsubject.id;
          public          postgres    false    229            �            1259    25058    django_admin_log    TABLE     �  CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);
 $   DROP TABLE public.django_admin_log;
       public         heap    postgres    false            �            1259    25056    django_admin_log_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.django_admin_log_id_seq;
       public          postgres    false    217            �           0    0    django_admin_log_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;
          public          postgres    false    216            �            1259    24944    django_content_type    TABLE     �   CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);
 '   DROP TABLE public.django_content_type;
       public         heap    postgres    false            �            1259    24942    django_content_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.django_content_type_id_seq;
       public          postgres    false    203            �           0    0    django_content_type_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;
          public          postgres    false    202            �            1259    24933    django_migrations    TABLE     �   CREATE TABLE public.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
 %   DROP TABLE public.django_migrations;
       public         heap    postgres    false            �            1259    24931    django_migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.django_migrations_id_seq;
       public          postgres    false    201            �           0    0    django_migrations_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;
          public          postgres    false    200            �            1259    25089    django_session    TABLE     �   CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);
 "   DROP TABLE public.django_session;
       public         heap    postgres    false            �            1259    32918    lesson_api_lesson    TABLE     (  CREATE TABLE public.lesson_api_lesson (
    id bigint NOT NULL,
    class_id_id bigint NOT NULL,
    classroom_id bigint NOT NULL,
    subject_id bigint NOT NULL,
    teacher_id bigint NOT NULL,
    timebox_id bigint NOT NULL,
    timetable_id bigint NOT NULL,
    week_day_id bigint NOT NULL
);
 %   DROP TABLE public.lesson_api_lesson;
       public         heap    postgres    false            �            1259    32916    lesson_api_lesson_id_seq    SEQUENCE     �   CREATE SEQUENCE public.lesson_api_lesson_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.lesson_api_lesson_id_seq;
       public          postgres    false    244            �           0    0    lesson_api_lesson_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.lesson_api_lesson_id_seq OWNED BY public.lesson_api_lesson.id;
          public          postgres    false    243            �            1259    32886    preference_api_preference    TABLE       CREATE TABLE public.preference_api_preference (
    id bigint NOT NULL,
    description character varying(255) NOT NULL,
    preference_type_id bigint NOT NULL,
    teacher_id bigint NOT NULL,
    timebox_id bigint NOT NULL,
    week_day_id bigint NOT NULL
);
 -   DROP TABLE public.preference_api_preference;
       public         heap    postgres    false            �            1259    32884     preference_api_preference_id_seq    SEQUENCE     �   CREATE SEQUENCE public.preference_api_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.preference_api_preference_id_seq;
       public          postgres    false    242            �           0    0     preference_api_preference_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.preference_api_preference_id_seq OWNED BY public.preference_api_preference.id;
          public          postgres    false    241            �            1259    32878 "   preference_type_api_preferencetype    TABLE     �   CREATE TABLE public.preference_type_api_preferencetype (
    id bigint NOT NULL,
    name character varying(50) NOT NULL,
    colour character varying(15) NOT NULL
);
 6   DROP TABLE public.preference_type_api_preferencetype;
       public         heap    postgres    false            �            1259    32876 )   preference_type_api_preferencetype_id_seq    SEQUENCE     �   CREATE SEQUENCE public.preference_type_api_preferencetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 @   DROP SEQUENCE public.preference_type_api_preferencetype_id_seq;
       public          postgres    false    240            �           0    0 )   preference_type_api_preferencetype_id_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public.preference_type_api_preferencetype_id_seq OWNED BY public.preference_type_api_preferencetype.id;
          public          postgres    false    239            �            1259    32770    subject_api_subject    TABLE     n   CREATE TABLE public.subject_api_subject (
    id bigint NOT NULL,
    name character varying(100) NOT NULL
);
 '   DROP TABLE public.subject_api_subject;
       public         heap    postgres    false            �            1259    32768    subject_api_subject_id_seq    SEQUENCE     �   CREATE SEQUENCE public.subject_api_subject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.subject_api_subject_id_seq;
       public          postgres    false    222            �           0    0    subject_api_subject_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.subject_api_subject_id_seq OWNED BY public.subject_api_subject.id;
          public          postgres    false    221            �            1259    25101    teacher_api_teacher    TABLE     �   CREATE TABLE public.teacher_api_teacher (
    id bigint NOT NULL,
    name character varying(20) NOT NULL,
    surname character varying(20) NOT NULL,
    email character varying(254) NOT NULL
);
 '   DROP TABLE public.teacher_api_teacher;
       public         heap    postgres    false            �            1259    25099    teacher_api_teachers_id_seq    SEQUENCE     �   CREATE SEQUENCE public.teacher_api_teachers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.teacher_api_teachers_id_seq;
       public          postgres    false    220            �           0    0    teacher_api_teachers_id_seq    SEQUENCE OWNED BY     Z   ALTER SEQUENCE public.teacher_api_teachers_id_seq OWNED BY public.teacher_api_teacher.id;
          public          postgres    false    219            �            1259    32778 "   teacher_subject_api_teachersubject    TABLE     �   CREATE TABLE public.teacher_subject_api_teachersubject (
    id bigint NOT NULL,
    subject_id bigint NOT NULL,
    teacher_id bigint NOT NULL
);
 6   DROP TABLE public.teacher_subject_api_teachersubject;
       public         heap    postgres    false            �            1259    32776 )   teacher_subject_api_teachersubject_id_seq    SEQUENCE     �   CREATE SEQUENCE public.teacher_subject_api_teachersubject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 @   DROP SEQUENCE public.teacher_subject_api_teachersubject_id_seq;
       public          postgres    false    224                        0    0 )   teacher_subject_api_teachersubject_id_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public.teacher_subject_api_teachersubject_id_seq OWNED BY public.teacher_subject_api_teachersubject.id;
          public          postgres    false    223            �            1259    32870    timebox_api_timebox    TABLE     �   CREATE TABLE public.timebox_api_timebox (
    id bigint NOT NULL,
    start time without time zone NOT NULL,
    stop time without time zone NOT NULL
);
 '   DROP TABLE public.timebox_api_timebox;
       public         heap    postgres    false            �            1259    32868    timebox_api_timebox_id_seq    SEQUENCE     �   CREATE SEQUENCE public.timebox_api_timebox_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.timebox_api_timebox_id_seq;
       public          postgres    false    238                       0    0    timebox_api_timebox_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.timebox_api_timebox_id_seq OWNED BY public.timebox_api_timebox.id;
          public          postgres    false    237            �            1259    32854    timetable_api_timetable    TABLE     �   CREATE TABLE public.timetable_api_timetable (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    creation_date date NOT NULL,
    date_of_acceptance date NOT NULL,
    is_accepted boolean NOT NULL
);
 +   DROP TABLE public.timetable_api_timetable;
       public         heap    postgres    false            �            1259    32852    timetable_api_timetable_id_seq    SEQUENCE     �   CREATE SEQUENCE public.timetable_api_timetable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.timetable_api_timetable_id_seq;
       public          postgres    false    234                       0    0    timetable_api_timetable_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.timetable_api_timetable_id_seq OWNED BY public.timetable_api_timetable.id;
          public          postgres    false    233            �            1259    32862    weekday_api_weekday    TABLE     m   CREATE TABLE public.weekday_api_weekday (
    id bigint NOT NULL,
    name character varying(15) NOT NULL
);
 '   DROP TABLE public.weekday_api_weekday;
       public         heap    postgres    false            �            1259    32860    weekday_api_weekday_id_seq    SEQUENCE     �   CREATE SEQUENCE public.weekday_api_weekday_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.weekday_api_weekday_id_seq;
       public          postgres    false    236                       0    0    weekday_api_weekday_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.weekday_api_weekday_id_seq OWNED BY public.weekday_api_weekday.id;
          public          postgres    false    235            �           2604    24965    auth_group id    DEFAULT     n   ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);
 <   ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206    207            �           2604    24975    auth_group_permissions id    DEFAULT     �   ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);
 H   ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    208    209    209            �           2604    24957    auth_permission id    DEFAULT     x   ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);
 A   ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    204    205    205            �           2604    24983    auth_user id    DEFAULT     l   ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);
 ;   ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    210    211    211            �           2604    24993    auth_user_groups id    DEFAULT     z   ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);
 B   ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    213    213            �           2604    25001    auth_user_user_permissions id    DEFAULT     �   ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);
 L   ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    215    214    215            �           2604    32801    building_api_building id    DEFAULT     �   ALTER TABLE ONLY public.building_api_building ALTER COLUMN id SET DEFAULT nextval('public.building_api_building_id_seq'::regclass);
 G   ALTER TABLE public.building_api_building ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    225    226    226            �           2604    32848    class_api_class id    DEFAULT     x   ALTER TABLE ONLY public.class_api_class ALTER COLUMN id SET DEFAULT nextval('public.class_api_class_id_seq'::regclass);
 A   ALTER TABLE public.class_api_class ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    232    231    232            �           2604    32809    classroom_api_classroom id    DEFAULT     �   ALTER TABLE ONLY public.classroom_api_classroom ALTER COLUMN id SET DEFAULT nextval('public.classroom_api_classroom_id_seq'::regclass);
 I   ALTER TABLE public.classroom_api_classroom ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    228    227    228            �           2604    32823 )   classroom_subject_api_classroomsubject id    DEFAULT     �   ALTER TABLE ONLY public.classroom_subject_api_classroomsubject ALTER COLUMN id SET DEFAULT nextval('public.classroom_subject_api_classroomsubject_id_seq'::regclass);
 X   ALTER TABLE public.classroom_subject_api_classroomsubject ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    230    229    230            �           2604    25061    django_admin_log id    DEFAULT     z   ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);
 B   ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    216    217    217            �           2604    24947    django_content_type id    DEFAULT     �   ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);
 E   ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    202    203    203            �           2604    24936    django_migrations id    DEFAULT     |   ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);
 C   ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    201    200    201            �           2604    32921    lesson_api_lesson id    DEFAULT     |   ALTER TABLE ONLY public.lesson_api_lesson ALTER COLUMN id SET DEFAULT nextval('public.lesson_api_lesson_id_seq'::regclass);
 C   ALTER TABLE public.lesson_api_lesson ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    244    243    244            �           2604    32889    preference_api_preference id    DEFAULT     �   ALTER TABLE ONLY public.preference_api_preference ALTER COLUMN id SET DEFAULT nextval('public.preference_api_preference_id_seq'::regclass);
 K   ALTER TABLE public.preference_api_preference ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    241    242    242            �           2604    32881 %   preference_type_api_preferencetype id    DEFAULT     �   ALTER TABLE ONLY public.preference_type_api_preferencetype ALTER COLUMN id SET DEFAULT nextval('public.preference_type_api_preferencetype_id_seq'::regclass);
 T   ALTER TABLE public.preference_type_api_preferencetype ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    240    239    240            �           2604    32773    subject_api_subject id    DEFAULT     �   ALTER TABLE ONLY public.subject_api_subject ALTER COLUMN id SET DEFAULT nextval('public.subject_api_subject_id_seq'::regclass);
 E   ALTER TABLE public.subject_api_subject ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    222    221    222            �           2604    25104    teacher_api_teacher id    DEFAULT     �   ALTER TABLE ONLY public.teacher_api_teacher ALTER COLUMN id SET DEFAULT nextval('public.teacher_api_teachers_id_seq'::regclass);
 E   ALTER TABLE public.teacher_api_teacher ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    219    220    220            �           2604    32781 %   teacher_subject_api_teachersubject id    DEFAULT     �   ALTER TABLE ONLY public.teacher_subject_api_teachersubject ALTER COLUMN id SET DEFAULT nextval('public.teacher_subject_api_teachersubject_id_seq'::regclass);
 T   ALTER TABLE public.teacher_subject_api_teachersubject ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    224    223    224            �           2604    32873    timebox_api_timebox id    DEFAULT     �   ALTER TABLE ONLY public.timebox_api_timebox ALTER COLUMN id SET DEFAULT nextval('public.timebox_api_timebox_id_seq'::regclass);
 E   ALTER TABLE public.timebox_api_timebox ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    238    237    238            �           2604    32857    timetable_api_timetable id    DEFAULT     �   ALTER TABLE ONLY public.timetable_api_timetable ALTER COLUMN id SET DEFAULT nextval('public.timetable_api_timetable_id_seq'::regclass);
 I   ALTER TABLE public.timetable_api_timetable ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    234    233    234            �           2604    32865    weekday_api_weekday id    DEFAULT     �   ALTER TABLE ONLY public.weekday_api_weekday ALTER COLUMN id SET DEFAULT nextval('public.weekday_api_weekday_id_seq'::regclass);
 E   ALTER TABLE public.weekday_api_weekday ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    236    235    236            �          0    24962 
   auth_group 
   TABLE DATA           .   COPY public.auth_group (id, name) FROM stdin;
    public          postgres    false    207   M1      �          0    24972    auth_group_permissions 
   TABLE DATA           M   COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
    public          postgres    false    209   j1      �          0    24954    auth_permission 
   TABLE DATA           N   COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
    public          postgres    false    205   �1      �          0    24980 	   auth_user 
   TABLE DATA           �   COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
    public          postgres    false    211   �4      �          0    24990    auth_user_groups 
   TABLE DATA           A   COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
    public          postgres    false    213   �4      �          0    24998    auth_user_user_permissions 
   TABLE DATA           P   COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
    public          postgres    false    215   �4      �          0    32798    building_api_building 
   TABLE DATA           b   COPY public.building_api_building (id, name, street, house_number, postal_code, city) FROM stdin;
    public          postgres    false    226   �4      �          0    32845    class_api_class 
   TABLE DATA           G   COPY public.class_api_class (id, name, number_of_students) FROM stdin;
    public          postgres    false    232   {5      �          0    32806    classroom_api_classroom 
   TABLE DATA           _   COPY public.classroom_api_classroom (id, name, capacity, description, building_id) FROM stdin;
    public          postgres    false    228   �5      �          0    32820 &   classroom_subject_api_classroomsubject 
   TABLE DATA           ^   COPY public.classroom_subject_api_classroomsubject (id, subject_id, classroom_id) FROM stdin;
    public          postgres    false    230   "6      �          0    25058    django_admin_log 
   TABLE DATA           �   COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
    public          postgres    false    217   E6      �          0    24944    django_content_type 
   TABLE DATA           C   COPY public.django_content_type (id, app_label, model) FROM stdin;
    public          postgres    false    203   b6      �          0    24933    django_migrations 
   TABLE DATA           C   COPY public.django_migrations (id, app, name, applied) FROM stdin;
    public          postgres    false    201   L7      �          0    25089    django_session 
   TABLE DATA           P   COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
    public          postgres    false    218   �:      �          0    32918    lesson_api_lesson 
   TABLE DATA           �   COPY public.lesson_api_lesson (id, class_id_id, classroom_id, subject_id, teacher_id, timebox_id, timetable_id, week_day_id) FROM stdin;
    public          postgres    false    244   �:      �          0    32886    preference_api_preference 
   TABLE DATA           }   COPY public.preference_api_preference (id, description, preference_type_id, teacher_id, timebox_id, week_day_id) FROM stdin;
    public          postgres    false    242   �:      �          0    32878 "   preference_type_api_preferencetype 
   TABLE DATA           N   COPY public.preference_type_api_preferencetype (id, name, colour) FROM stdin;
    public          postgres    false    240   3;      �          0    32770    subject_api_subject 
   TABLE DATA           7   COPY public.subject_api_subject (id, name) FROM stdin;
    public          postgres    false    222   �;      �          0    25101    teacher_api_teacher 
   TABLE DATA           G   COPY public.teacher_api_teacher (id, name, surname, email) FROM stdin;
    public          postgres    false    220   �;      �          0    32778 "   teacher_subject_api_teachersubject 
   TABLE DATA           X   COPY public.teacher_subject_api_teachersubject (id, subject_id, teacher_id) FROM stdin;
    public          postgres    false    224   H<      �          0    32870    timebox_api_timebox 
   TABLE DATA           >   COPY public.timebox_api_timebox (id, start, stop) FROM stdin;
    public          postgres    false    238   o<      �          0    32854    timetable_api_timetable 
   TABLE DATA           k   COPY public.timetable_api_timetable (id, name, creation_date, date_of_acceptance, is_accepted) FROM stdin;
    public          postgres    false    234   �<      �          0    32862    weekday_api_weekday 
   TABLE DATA           7   COPY public.weekday_api_weekday (id, name) FROM stdin;
    public          postgres    false    236   @=                 0    0    auth_group_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);
          public          postgres    false    206                       0    0    auth_group_permissions_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);
          public          postgres    false    208                       0    0    auth_permission_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.auth_permission_id_seq', 80, true);
          public          postgres    false    204                       0    0    auth_user_groups_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);
          public          postgres    false    212                       0    0    auth_user_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.auth_user_id_seq', 1, false);
          public          postgres    false    210            	           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);
          public          postgres    false    214            
           0    0    building_api_building_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.building_api_building_id_seq', 9, true);
          public          postgres    false    225                       0    0    class_api_class_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.class_api_class_id_seq', 4, true);
          public          postgres    false    231                       0    0    classroom_api_classroom_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.classroom_api_classroom_id_seq', 6, true);
          public          postgres    false    227                       0    0 -   classroom_subject_api_classroomsubject_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.classroom_subject_api_classroomsubject_id_seq', 2, true);
          public          postgres    false    229                       0    0    django_admin_log_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);
          public          postgres    false    216                       0    0    django_content_type_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.django_content_type_id_seq', 19, true);
          public          postgres    false    202                       0    0    django_migrations_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.django_migrations_id_seq', 38, true);
          public          postgres    false    200                       0    0    lesson_api_lesson_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.lesson_api_lesson_id_seq', 10, true);
          public          postgres    false    243                       0    0     preference_api_preference_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.preference_api_preference_id_seq', 2, true);
          public          postgres    false    241                       0    0 )   preference_type_api_preferencetype_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.preference_type_api_preferencetype_id_seq', 3, true);
          public          postgres    false    239                       0    0    subject_api_subject_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.subject_api_subject_id_seq', 4, true);
          public          postgres    false    221                       0    0    teacher_api_teachers_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.teacher_api_teachers_id_seq', 6, true);
          public          postgres    false    219                       0    0 )   teacher_subject_api_teachersubject_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.teacher_subject_api_teachersubject_id_seq', 4, true);
          public          postgres    false    223                       0    0    timebox_api_timebox_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.timebox_api_timebox_id_seq', 10, true);
          public          postgres    false    237                       0    0    timetable_api_timetable_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.timetable_api_timetable_id_seq', 6, true);
          public          postgres    false    233                       0    0    weekday_api_weekday_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.weekday_api_weekday_id_seq', 5, true);
          public          postgres    false    235            �           2606    25087    auth_group auth_group_name_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);
 H   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
       public            postgres    false    207            �           2606    25014 R   auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);
 |   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
       public            postgres    false    209    209            �           2606    24977 2   auth_group_permissions auth_group_permissions_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
       public            postgres    false    209            �           2606    24967    auth_group auth_group_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
       public            postgres    false    207            �           2606    25005 F   auth_permission auth_permission_content_type_id_codename_01ab375a_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);
 p   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
       public            postgres    false    205    205            �           2606    24959 $   auth_permission auth_permission_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
       public            postgres    false    205            �           2606    24995 &   auth_user_groups auth_user_groups_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
       public            postgres    false    213            �           2606    25029 @   auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);
 j   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
       public            postgres    false    213    213            �           2606    24985    auth_user auth_user_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
       public            postgres    false    211            �           2606    25003 :   auth_user_user_permissions auth_user_user_permissions_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
       public            postgres    false    215            �           2606    25043 Y   auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
       public            postgres    false    215    215            �           2606    25081     auth_user auth_user_username_key 
   CONSTRAINT     _   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);
 J   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
       public            postgres    false    211            �           2606    32803 0   building_api_building building_api_building_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.building_api_building
    ADD CONSTRAINT building_api_building_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.building_api_building DROP CONSTRAINT building_api_building_pkey;
       public            postgres    false    226                       2606    32850 $   class_api_class class_api_class_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.class_api_class
    ADD CONSTRAINT class_api_class_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.class_api_class DROP CONSTRAINT class_api_class_pkey;
       public            postgres    false    232                       2606    32811 4   classroom_api_classroom classroom_api_classroom_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.classroom_api_classroom
    ADD CONSTRAINT classroom_api_classroom_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.classroom_api_classroom DROP CONSTRAINT classroom_api_classroom_pkey;
       public            postgres    false    228                       2606    32825 R   classroom_subject_api_classroomsubject classroom_subject_api_classroomsubject_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.classroom_subject_api_classroomsubject
    ADD CONSTRAINT classroom_subject_api_classroomsubject_pkey PRIMARY KEY (id);
 |   ALTER TABLE ONLY public.classroom_subject_api_classroomsubject DROP CONSTRAINT classroom_subject_api_classroomsubject_pkey;
       public            postgres    false    230            �           2606    25067 &   django_admin_log django_admin_log_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
       public            postgres    false    217            �           2606    24951 E   django_content_type django_content_type_app_label_model_76bd3d3b_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);
 o   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
       public            postgres    false    203    203            �           2606    24949 ,   django_content_type django_content_type_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
       public            postgres    false    203            �           2606    24941 (   django_migrations django_migrations_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
       public            postgres    false    201            �           2606    25096 "   django_session django_session_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);
 L   ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
       public            postgres    false    218                       2606    32923 (   lesson_api_lesson lesson_api_lesson_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.lesson_api_lesson
    ADD CONSTRAINT lesson_api_lesson_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.lesson_api_lesson DROP CONSTRAINT lesson_api_lesson_pkey;
       public            postgres    false    244                       2606    32891 8   preference_api_preference preference_api_preference_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.preference_api_preference
    ADD CONSTRAINT preference_api_preference_pkey PRIMARY KEY (id);
 b   ALTER TABLE ONLY public.preference_api_preference DROP CONSTRAINT preference_api_preference_pkey;
       public            postgres    false    242                       2606    32883 J   preference_type_api_preferencetype preference_type_api_preferencetype_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.preference_type_api_preferencetype
    ADD CONSTRAINT preference_type_api_preferencetype_pkey PRIMARY KEY (id);
 t   ALTER TABLE ONLY public.preference_type_api_preferencetype DROP CONSTRAINT preference_type_api_preferencetype_pkey;
       public            postgres    false    240            �           2606    32775 ,   subject_api_subject subject_api_subject_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.subject_api_subject
    ADD CONSTRAINT subject_api_subject_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.subject_api_subject DROP CONSTRAINT subject_api_subject_pkey;
       public            postgres    false    222            �           2606    25106 -   teacher_api_teacher teacher_api_teachers_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.teacher_api_teacher
    ADD CONSTRAINT teacher_api_teachers_pkey PRIMARY KEY (id);
 W   ALTER TABLE ONLY public.teacher_api_teacher DROP CONSTRAINT teacher_api_teachers_pkey;
       public            postgres    false    220            �           2606    32783 J   teacher_subject_api_teachersubject teacher_subject_api_teachersubject_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.teacher_subject_api_teachersubject
    ADD CONSTRAINT teacher_subject_api_teachersubject_pkey PRIMARY KEY (id);
 t   ALTER TABLE ONLY public.teacher_subject_api_teachersubject DROP CONSTRAINT teacher_subject_api_teachersubject_pkey;
       public            postgres    false    224                       2606    32875 ,   timebox_api_timebox timebox_api_timebox_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.timebox_api_timebox
    ADD CONSTRAINT timebox_api_timebox_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.timebox_api_timebox DROP CONSTRAINT timebox_api_timebox_pkey;
       public            postgres    false    238            
           2606    32859 4   timetable_api_timetable timetable_api_timetable_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.timetable_api_timetable
    ADD CONSTRAINT timetable_api_timetable_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.timetable_api_timetable DROP CONSTRAINT timetable_api_timetable_pkey;
       public            postgres    false    234                       2606    32867 ,   weekday_api_weekday weekday_api_weekday_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.weekday_api_weekday
    ADD CONSTRAINT weekday_api_weekday_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.weekday_api_weekday DROP CONSTRAINT weekday_api_weekday_pkey;
       public            postgres    false    236            �           1259    25088    auth_group_name_a6ea08ec_like    INDEX     h   CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);
 1   DROP INDEX public.auth_group_name_a6ea08ec_like;
       public            postgres    false    207            �           1259    25025 (   auth_group_permissions_group_id_b120cbf9    INDEX     o   CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);
 <   DROP INDEX public.auth_group_permissions_group_id_b120cbf9;
       public            postgres    false    209            �           1259    25026 -   auth_group_permissions_permission_id_84c5c92e    INDEX     y   CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);
 A   DROP INDEX public.auth_group_permissions_permission_id_84c5c92e;
       public            postgres    false    209            �           1259    25011 (   auth_permission_content_type_id_2f476e4b    INDEX     o   CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);
 <   DROP INDEX public.auth_permission_content_type_id_2f476e4b;
       public            postgres    false    205            �           1259    25041 "   auth_user_groups_group_id_97559544    INDEX     c   CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);
 6   DROP INDEX public.auth_user_groups_group_id_97559544;
       public            postgres    false    213            �           1259    25040 !   auth_user_groups_user_id_6a12ed8b    INDEX     a   CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);
 5   DROP INDEX public.auth_user_groups_user_id_6a12ed8b;
       public            postgres    false    213            �           1259    25055 1   auth_user_user_permissions_permission_id_1fbb5f2c    INDEX     �   CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);
 E   DROP INDEX public.auth_user_user_permissions_permission_id_1fbb5f2c;
       public            postgres    false    215            �           1259    25054 +   auth_user_user_permissions_user_id_a95ead1b    INDEX     u   CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);
 ?   DROP INDEX public.auth_user_user_permissions_user_id_a95ead1b;
       public            postgres    false    215            �           1259    25082     auth_user_username_6821ab7c_like    INDEX     n   CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);
 4   DROP INDEX public.auth_user_username_6821ab7c_like;
       public            postgres    false    211                        1259    32817 ,   classroom_api_classroom_building_id_a9347aff    INDEX     w   CREATE INDEX classroom_api_classroom_building_id_a9347aff ON public.classroom_api_classroom USING btree (building_id);
 @   DROP INDEX public.classroom_api_classroom_building_id_a9347aff;
       public            postgres    false    228                       1259    32836 :   classroom_subject_api_classroomsubject_subject_id_bfd7e995    INDEX     �   CREATE INDEX classroom_subject_api_classroomsubject_subject_id_bfd7e995 ON public.classroom_subject_api_classroomsubject USING btree (subject_id);
 N   DROP INDEX public.classroom_subject_api_classroomsubject_subject_id_bfd7e995;
       public            postgres    false    230                       1259    32837 :   classroom_subject_api_classroomsubject_teacher_id_909d0468    INDEX     �   CREATE INDEX classroom_subject_api_classroomsubject_teacher_id_909d0468 ON public.classroom_subject_api_classroomsubject USING btree (classroom_id);
 N   DROP INDEX public.classroom_subject_api_classroomsubject_teacher_id_909d0468;
       public            postgres    false    230            �           1259    25078 )   django_admin_log_content_type_id_c4bce8eb    INDEX     q   CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);
 =   DROP INDEX public.django_admin_log_content_type_id_c4bce8eb;
       public            postgres    false    217            �           1259    25079 !   django_admin_log_user_id_c564eba6    INDEX     a   CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);
 5   DROP INDEX public.django_admin_log_user_id_c564eba6;
       public            postgres    false    217            �           1259    25098 #   django_session_expire_date_a5c62663    INDEX     e   CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);
 7   DROP INDEX public.django_session_expire_date_a5c62663;
       public            postgres    false    218            �           1259    25097 (   django_session_session_key_c0390e0f_like    INDEX     ~   CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);
 <   DROP INDEX public.django_session_session_key_c0390e0f_like;
       public            postgres    false    218                       1259    32959 &   lesson_api_lesson_class_id_id_50cc90c3    INDEX     k   CREATE INDEX lesson_api_lesson_class_id_id_50cc90c3 ON public.lesson_api_lesson USING btree (class_id_id);
 :   DROP INDEX public.lesson_api_lesson_class_id_id_50cc90c3;
       public            postgres    false    244                       1259    32960 '   lesson_api_lesson_classroom_id_c1c434d7    INDEX     m   CREATE INDEX lesson_api_lesson_classroom_id_c1c434d7 ON public.lesson_api_lesson USING btree (classroom_id);
 ;   DROP INDEX public.lesson_api_lesson_classroom_id_c1c434d7;
       public            postgres    false    244                       1259    32961 %   lesson_api_lesson_subject_id_4818b1a1    INDEX     i   CREATE INDEX lesson_api_lesson_subject_id_4818b1a1 ON public.lesson_api_lesson USING btree (subject_id);
 9   DROP INDEX public.lesson_api_lesson_subject_id_4818b1a1;
       public            postgres    false    244                       1259    32962 %   lesson_api_lesson_teacher_id_13315c12    INDEX     i   CREATE INDEX lesson_api_lesson_teacher_id_13315c12 ON public.lesson_api_lesson USING btree (teacher_id);
 9   DROP INDEX public.lesson_api_lesson_teacher_id_13315c12;
       public            postgres    false    244                       1259    32963 %   lesson_api_lesson_timebox_id_9bc530b2    INDEX     i   CREATE INDEX lesson_api_lesson_timebox_id_9bc530b2 ON public.lesson_api_lesson USING btree (timebox_id);
 9   DROP INDEX public.lesson_api_lesson_timebox_id_9bc530b2;
       public            postgres    false    244                       1259    32964 '   lesson_api_lesson_timetable_id_4efff65e    INDEX     m   CREATE INDEX lesson_api_lesson_timetable_id_4efff65e ON public.lesson_api_lesson USING btree (timetable_id);
 ;   DROP INDEX public.lesson_api_lesson_timetable_id_4efff65e;
       public            postgres    false    244                       1259    32965 &   lesson_api_lesson_week_day_id_e6741fb6    INDEX     k   CREATE INDEX lesson_api_lesson_week_day_id_e6741fb6 ON public.lesson_api_lesson USING btree (week_day_id);
 :   DROP INDEX public.lesson_api_lesson_week_day_id_e6741fb6;
       public            postgres    false    244                       1259    32912 5   preference_api_preference_preference_type_id_55dff8f1    INDEX     �   CREATE INDEX preference_api_preference_preference_type_id_55dff8f1 ON public.preference_api_preference USING btree (preference_type_id);
 I   DROP INDEX public.preference_api_preference_preference_type_id_55dff8f1;
       public            postgres    false    242                       1259    32913 -   preference_api_preference_teacher_id_5f6ec422    INDEX     y   CREATE INDEX preference_api_preference_teacher_id_5f6ec422 ON public.preference_api_preference USING btree (teacher_id);
 A   DROP INDEX public.preference_api_preference_teacher_id_5f6ec422;
       public            postgres    false    242                       1259    32914 -   preference_api_preference_timebox_id_010c30ab    INDEX     y   CREATE INDEX preference_api_preference_timebox_id_010c30ab ON public.preference_api_preference USING btree (timebox_id);
 A   DROP INDEX public.preference_api_preference_timebox_id_010c30ab;
       public            postgres    false    242                       1259    32915 .   preference_api_preference_week_day_id_712a0af2    INDEX     {   CREATE INDEX preference_api_preference_week_day_id_712a0af2 ON public.preference_api_preference USING btree (week_day_id);
 B   DROP INDEX public.preference_api_preference_week_day_id_712a0af2;
       public            postgres    false    242            �           1259    32794 6   teacher_subject_api_teachersubject_subject_id_7e36c392    INDEX     �   CREATE INDEX teacher_subject_api_teachersubject_subject_id_7e36c392 ON public.teacher_subject_api_teachersubject USING btree (subject_id);
 J   DROP INDEX public.teacher_subject_api_teachersubject_subject_id_7e36c392;
       public            postgres    false    224            �           1259    32795 6   teacher_subject_api_teachersubject_teacher_id_b7b1ffe2    INDEX     �   CREATE INDEX teacher_subject_api_teachersubject_teacher_id_b7b1ffe2 ON public.teacher_subject_api_teachersubject USING btree (teacher_id);
 J   DROP INDEX public.teacher_subject_api_teachersubject_teacher_id_b7b1ffe2;
       public            postgres    false    224            "           2606    25020 O   auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 y   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
       public          postgres    false    3025    209    205            !           2606    25015 P   auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
       public          postgres    false    207    3030    209                        2606    25006 E   auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 o   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
       public          postgres    false    205    3020    203            $           2606    25035 D   auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 n   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
       public          postgres    false    213    3030    207            #           2606    25030 B   auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
       public          postgres    false    3038    211    213            &           2606    25049 S   auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 }   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
       public          postgres    false    205    215    3025            %           2606    25044 V   auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
       public          postgres    false    215    3038    211            +           2606    32812 N   classroom_api_classroom classroom_api_classr_building_id_a9347aff_fk_building_    FK CONSTRAINT     �   ALTER TABLE ONLY public.classroom_api_classroom
    ADD CONSTRAINT classroom_api_classr_building_id_a9347aff_fk_building_ FOREIGN KEY (building_id) REFERENCES public.building_api_building(id) DEFERRABLE INITIALLY DEFERRED;
 x   ALTER TABLE ONLY public.classroom_api_classroom DROP CONSTRAINT classroom_api_classr_building_id_a9347aff_fk_building_;
       public          postgres    false    3071    226    228            -           2606    32838 ^   classroom_subject_api_classroomsubject classroom_subject_ap_classroom_id_058fdd85_fk_classroom    FK CONSTRAINT     �   ALTER TABLE ONLY public.classroom_subject_api_classroomsubject
    ADD CONSTRAINT classroom_subject_ap_classroom_id_058fdd85_fk_classroom FOREIGN KEY (classroom_id) REFERENCES public.classroom_api_classroom(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.classroom_subject_api_classroomsubject DROP CONSTRAINT classroom_subject_ap_classroom_id_058fdd85_fk_classroom;
       public          postgres    false    3074    228    230            ,           2606    32826 \   classroom_subject_api_classroomsubject classroom_subject_ap_subject_id_bfd7e995_fk_subject_a    FK CONSTRAINT     �   ALTER TABLE ONLY public.classroom_subject_api_classroomsubject
    ADD CONSTRAINT classroom_subject_ap_subject_id_bfd7e995_fk_subject_a FOREIGN KEY (subject_id) REFERENCES public.subject_api_subject(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.classroom_subject_api_classroomsubject DROP CONSTRAINT classroom_subject_ap_subject_id_bfd7e995_fk_subject_a;
       public          postgres    false    3065    230    222            '           2606    25068 G   django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 q   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
       public          postgres    false    3020    203    217            (           2606    25073 B   django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
       public          postgres    false    3038    211    217            2           2606    32924 N   lesson_api_lesson lesson_api_lesson_class_id_id_50cc90c3_fk_class_api_class_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.lesson_api_lesson
    ADD CONSTRAINT lesson_api_lesson_class_id_id_50cc90c3_fk_class_api_class_id FOREIGN KEY (class_id_id) REFERENCES public.class_api_class(id) DEFERRABLE INITIALLY DEFERRED;
 x   ALTER TABLE ONLY public.lesson_api_lesson DROP CONSTRAINT lesson_api_lesson_class_id_id_50cc90c3_fk_class_api_class_id;
       public          postgres    false    244    3080    232            3           2606    32929 F   lesson_api_lesson lesson_api_lesson_classroom_id_c1c434d7_fk_classroom    FK CONSTRAINT     �   ALTER TABLE ONLY public.lesson_api_lesson
    ADD CONSTRAINT lesson_api_lesson_classroom_id_c1c434d7_fk_classroom FOREIGN KEY (classroom_id) REFERENCES public.classroom_api_classroom(id) DEFERRABLE INITIALLY DEFERRED;
 p   ALTER TABLE ONLY public.lesson_api_lesson DROP CONSTRAINT lesson_api_lesson_classroom_id_c1c434d7_fk_classroom;
       public          postgres    false    244    3074    228            4           2606    32934 Q   lesson_api_lesson lesson_api_lesson_subject_id_4818b1a1_fk_subject_api_subject_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.lesson_api_lesson
    ADD CONSTRAINT lesson_api_lesson_subject_id_4818b1a1_fk_subject_api_subject_id FOREIGN KEY (subject_id) REFERENCES public.subject_api_subject(id) DEFERRABLE INITIALLY DEFERRED;
 {   ALTER TABLE ONLY public.lesson_api_lesson DROP CONSTRAINT lesson_api_lesson_subject_id_4818b1a1_fk_subject_api_subject_id;
       public          postgres    false    222    3065    244            5           2606    32939 Q   lesson_api_lesson lesson_api_lesson_teacher_id_13315c12_fk_teacher_api_teacher_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.lesson_api_lesson
    ADD CONSTRAINT lesson_api_lesson_teacher_id_13315c12_fk_teacher_api_teacher_id FOREIGN KEY (teacher_id) REFERENCES public.teacher_api_teacher(id) DEFERRABLE INITIALLY DEFERRED;
 {   ALTER TABLE ONLY public.lesson_api_lesson DROP CONSTRAINT lesson_api_lesson_teacher_id_13315c12_fk_teacher_api_teacher_id;
       public          postgres    false    220    244    3063            6           2606    32944 Q   lesson_api_lesson lesson_api_lesson_timebox_id_9bc530b2_fk_timebox_api_timebox_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.lesson_api_lesson
    ADD CONSTRAINT lesson_api_lesson_timebox_id_9bc530b2_fk_timebox_api_timebox_id FOREIGN KEY (timebox_id) REFERENCES public.timebox_api_timebox(id) DEFERRABLE INITIALLY DEFERRED;
 {   ALTER TABLE ONLY public.lesson_api_lesson DROP CONSTRAINT lesson_api_lesson_timebox_id_9bc530b2_fk_timebox_api_timebox_id;
       public          postgres    false    3086    238    244            7           2606    32949 F   lesson_api_lesson lesson_api_lesson_timetable_id_4efff65e_fk_timetable    FK CONSTRAINT     �   ALTER TABLE ONLY public.lesson_api_lesson
    ADD CONSTRAINT lesson_api_lesson_timetable_id_4efff65e_fk_timetable FOREIGN KEY (timetable_id) REFERENCES public.timetable_api_timetable(id) DEFERRABLE INITIALLY DEFERRED;
 p   ALTER TABLE ONLY public.lesson_api_lesson DROP CONSTRAINT lesson_api_lesson_timetable_id_4efff65e_fk_timetable;
       public          postgres    false    3082    244    234            8           2606    32954 E   lesson_api_lesson lesson_api_lesson_week_day_id_e6741fb6_fk_weekday_a    FK CONSTRAINT     �   ALTER TABLE ONLY public.lesson_api_lesson
    ADD CONSTRAINT lesson_api_lesson_week_day_id_e6741fb6_fk_weekday_a FOREIGN KEY (week_day_id) REFERENCES public.weekday_api_weekday(id) DEFERRABLE INITIALLY DEFERRED;
 o   ALTER TABLE ONLY public.lesson_api_lesson DROP CONSTRAINT lesson_api_lesson_week_day_id_e6741fb6_fk_weekday_a;
       public          postgres    false    3084    244    236            .           2606    32892 W   preference_api_preference preference_api_prefe_preference_type_id_55dff8f1_fk_preferenc    FK CONSTRAINT     �   ALTER TABLE ONLY public.preference_api_preference
    ADD CONSTRAINT preference_api_prefe_preference_type_id_55dff8f1_fk_preferenc FOREIGN KEY (preference_type_id) REFERENCES public.preference_type_api_preferencetype(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.preference_api_preference DROP CONSTRAINT preference_api_prefe_preference_type_id_55dff8f1_fk_preferenc;
       public          postgres    false    3088    240    242            /           2606    32897 O   preference_api_preference preference_api_prefe_teacher_id_5f6ec422_fk_teacher_a    FK CONSTRAINT     �   ALTER TABLE ONLY public.preference_api_preference
    ADD CONSTRAINT preference_api_prefe_teacher_id_5f6ec422_fk_teacher_a FOREIGN KEY (teacher_id) REFERENCES public.teacher_api_teacher(id) DEFERRABLE INITIALLY DEFERRED;
 y   ALTER TABLE ONLY public.preference_api_preference DROP CONSTRAINT preference_api_prefe_teacher_id_5f6ec422_fk_teacher_a;
       public          postgres    false    242    3063    220            0           2606    32902 O   preference_api_preference preference_api_prefe_timebox_id_010c30ab_fk_timebox_a    FK CONSTRAINT     �   ALTER TABLE ONLY public.preference_api_preference
    ADD CONSTRAINT preference_api_prefe_timebox_id_010c30ab_fk_timebox_a FOREIGN KEY (timebox_id) REFERENCES public.timebox_api_timebox(id) DEFERRABLE INITIALLY DEFERRED;
 y   ALTER TABLE ONLY public.preference_api_preference DROP CONSTRAINT preference_api_prefe_timebox_id_010c30ab_fk_timebox_a;
       public          postgres    false    242    3086    238            1           2606    32907 P   preference_api_preference preference_api_prefe_week_day_id_712a0af2_fk_weekday_a    FK CONSTRAINT     �   ALTER TABLE ONLY public.preference_api_preference
    ADD CONSTRAINT preference_api_prefe_week_day_id_712a0af2_fk_weekday_a FOREIGN KEY (week_day_id) REFERENCES public.weekday_api_weekday(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY public.preference_api_preference DROP CONSTRAINT preference_api_prefe_week_day_id_712a0af2_fk_weekday_a;
       public          postgres    false    242    3084    236            )           2606    32784 X   teacher_subject_api_teachersubject teacher_subject_api__subject_id_7e36c392_fk_subject_a    FK CONSTRAINT     �   ALTER TABLE ONLY public.teacher_subject_api_teachersubject
    ADD CONSTRAINT teacher_subject_api__subject_id_7e36c392_fk_subject_a FOREIGN KEY (subject_id) REFERENCES public.subject_api_subject(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.teacher_subject_api_teachersubject DROP CONSTRAINT teacher_subject_api__subject_id_7e36c392_fk_subject_a;
       public          postgres    false    224    3065    222            *           2606    32789 X   teacher_subject_api_teachersubject teacher_subject_api__teacher_id_b7b1ffe2_fk_teacher_a    FK CONSTRAINT     �   ALTER TABLE ONLY public.teacher_subject_api_teachersubject
    ADD CONSTRAINT teacher_subject_api__teacher_id_b7b1ffe2_fk_teacher_a FOREIGN KEY (teacher_id) REFERENCES public.teacher_api_teacher(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.teacher_subject_api_teachersubject DROP CONSTRAINT teacher_subject_api__teacher_id_b7b1ffe2_fk_teacher_a;
       public          postgres    false    220    224    3063            �      x������ � �      �      x������ � �      �   �  x�m�[��0E�ի�
Ro���T�0V<$\�3�����[���KG���V߻����q~\̴-_J���so�&d�?��a��-x7��$�,,�w0�;:*���,�a]�yR�U���� ю�5�,
2�!EAZGƵ����ʭ������s�BDs'��"�x�Z��<�W���
?A|||��;��#��;�����=����<m��]���Q�5���t�E"��r��G?�gWҌh�+X�W�v���؂L�wb�3`��zcg� k:�������J�	�8�B)�g j�8ìAml&� �&�NZ���I
���)AN�}}�~�~S��=ׂ\�yb��yL�|���y�����y���(���D�7�%c%ю��6���*�^�*��e5b�}�����0=��;���P��p݉�$��b�9�H%��u]������B",��bE^��3Ĳ�ҽc�kϖa�u&m�פ4���C�N΁��s	�YbJ�PN-��r�J�4+(��T��CY���{BYErABP��O�u��(�����}Ԉ�)�X`+�-N�XG��4�Ͻۿt��M�D��~�ib��e�Q�+0�X���O�*��ބ���!�j���5C�����^H�ղ%絘_f1So�M���C��2�Ej5�#0*�I�������%Q�>��<1#��h2P��iP�i��>�C]��62��5�'���vw�kA-�D��#��HQ�_���b�_ G�� ��.F�      �      x������ � �      �      x������ � �      �      x������ � �      �   �   x�3�O,*�*I,�t,.)��K,-9������H��Ĕ3 �*/�h�9����sUbQ^~yfjVqv"P	������!�wQb6P�%���& #�R��4�2/5��;?'�(�<��Ę��H���bgby"W� s�*�      �   )   x�3���Tp�4��2��t�42�2�x*�r�r��qqq e�      �   ^   x�3�42q�42�N�ITH�W�:2��2;Q!1/=35�8;35=�ӌ˔Ӕ���*?E!#��$�(3�ӒˌӘ��.�X���XR������� �h�      �      x�3�4�4����� �Z      �      x������ � �      �   �   x�U��n!D�폩���/�"�u6�, .J�ﻅ����9c�2�ge@ۉM��@�� �~V!(kp_�����PL
��Қ����q�� p�U�"�`NU�i�fs�i��nO��������̔q5HR���s&�!�:��ni۾/ً" ���4���C:�gK�F:��`[�Hgp����H��?,�W�H���^@��^kME��"��p�!      �   7  x����r�0���S��FZ�x��hd,;j�Pi��]$�&!7�`�]���M��ڻk���%�j���ޖ	�/D�P�H.tNh
4�T�$p����~�B:F�Ğ*��i�c���)���L����_{:m3� "�w�30�آ��՜K{1�k�T`��I�,�$��L���\m�6Q�6��E?0��]k�V��ƒ�xS�wS����D	�QjA�	5t��*��=��f�@��?RƟPN����H*$�F%C<2J����w6��M��R�@$��w���n�݌�m3D61�әM���ҟl_�]�kۺ5��:{���aӬS��Ԧ6;"g���t��~�[$A 6��EɄ����|�c�"�`�Tș���o�-�$N2]F���ξ�殔�"x���MIw}B�L�t�;[�bV���#!,g���dJ@֑�EO���gM�9���@�$6{ü��߮X#$����$f<Z+$ӛ_�~P�"�B�B	�0�d�k� dȭ����'���'�|
9�9c)F�,
 ��k�ڋe�k�n��r-�lB�rp�m
�B��졂o	 9ǁ�)�Z��a�>g�G�,o�/.�,8,"UR
��e�:��������8��V�#�S�M�'t�n�#o2"u������X���`9��e�K���sN��	:@�ጅ�b,d=��{�L�c[�+�^�4�;;l@��%�C@�ǻ�dB���H�	��� B�LJ�%���Ĕ�L���Fodٳ̳I����٢pMo��T\��w��AX�tJg�^���` �&�9�Tq :H�+=�/��      �      x������ � �      �   8   x���	 0�ZL��zI�u�,".G��P���DJ�uJ������|<6�      �   +   x�3�t�H>2S!73�H�BAQUjQ����F��@l����� ��      �   A   x�3�t�82�$/3�ӽ(55�ˈ�/35&����_�e��?�''�<���#�A�)\1z\\\ c�      �   =   x�3��M,I�M,��N�2�tˬ1�9������3Ss��3�L8=2�K�2�b���� %�:      �   g   x�3��M,J����/O�)����J̋φrRs3s���s��8��8��s8SR�
r�LA:3K��z�rR�3���*��KJSR�J��3�RSJA:b���� �A&A      �      x�3�4�4�2�4�1z\\\ U      �   _   x�5��� �RL���_G8 ��uo޾ؙ�@
4�v�#в��v7����iwP˙K���B�;�µe�a�����q<�{� �      �   R   x�3��I�S�J�:2�H�BP~�BpUv~N^������>�0TN�M-.)R����/�N�������1�i�klș����� ��      �   @   x�3����LM��L<ڔ��e�^�_ds�U����e��\U�XT3��<�
b��qqq }�     