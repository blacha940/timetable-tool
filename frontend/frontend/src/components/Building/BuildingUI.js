import React, { Component } from 'react';
import BuildingCardUI from '../Card/BuildingCardUI';
import AddBuildingCardUI from '../Card/AddBuildingCardUI';


class BuildingUI extends Component {

    state = {
        buildings: []
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/buildnigs/')
        .then((response) => response.json())
        .then(buildingList => {
            this.setState({ buildings: buildingList });
        });
    }

    render() {
        return (<div>
            {this.state.buildings.map((building) => (
                <BuildingCardUI key = {building.id} object = {building} img = './images/Building.png'/>
            ))}
            <AddBuildingCardUI/>
            </div>
        )
  }
}

export default BuildingUI