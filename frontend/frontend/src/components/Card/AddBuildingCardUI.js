import React, { Component } from 'react';
import { Card, Button, Modal, Form } from 'react-bootstrap';

class AddBuildingCardUI extends Component {

    constructor(){
        super();
        this.state={
            name: "",
            street: "",
            house_number: "",
            postal_code: "",
            city: "",
            show:false
        }
        this.changeHandler=this.changeHandler.bind(this);
        this.submitForm=this.submitForm.bind(this);
    }

    changeHandler(event){
        this.setState({
            [event.target.name]:event.target.value
        });
    }

    submitForm(){
        fetch('http://localhost:8000/api/buildnigs/',{
            method:'POST',
            body:JSON.stringify(this.state),
            headers:{
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then(response=>response.json())
        .then((data)=>console.log(data));

        this.setState({
            name: "",
            street: "",
            house_number: "",
            postal_code: "",
            city: "",
        });
    }

    handleModal(){
        this.setState({show: !this.state.show})
    }

    render() {
        return (
        <Card className="text-center" style={{ width: '18rem' }}>
            <Card.Img variant="top" src='./images/Add.png' />
            <Card.Body>
                <Button variant="outline-warning" onClick={()=>{this.handleModal()}}>Dodaj Budynek</Button>
                <Modal show={this.state.show} onHide={()=>this.handleModal()} animation={false}>
                <Modal.Header closeButton>
                <Modal.Title>Dodaj Budynek</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group >
                        <Form.Label>Nazwa: </Form.Label>
                        <Form.Control value={this.state.name} name="name" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Ulica: </Form.Label>
                        <Form.Control value={this.state.street} name="street" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                   <Form.Group >
                        <Form.Label>Numer: </Form.Label>
                        <Form.Control value={this.state.house_number} name="house_number" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Kod pocztowy: </Form.Label>
                        <Form.Control value={this.state.postal_code} name="postal_code" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Miasto: </Form.Label>
                        <Form.Control value={this.state.city} name="city" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type="submit" onClick={this.submitForm}>
                        Dodaj
                    </Button>
                </Modal.Footer>
            </Modal>
            </Card.Body>
        </Card>
        )
    }
}

export default AddBuildingCardUI