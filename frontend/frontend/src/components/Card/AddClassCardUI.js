import React, { Component } from 'react';
import { Card, Button, Modal, Form } from 'react-bootstrap';

class AddClassCardUI extends Component {

    constructor(){
        super();
        this.state={
            name:'',
            number_of_students: '',
            show:false
        }
        this.changeHandler=this.changeHandler.bind(this);
        this.submitForm=this.submitForm.bind(this);
    }

    changeHandler(event){
        this.setState({
            [event.target.name]:event.target.value
        });
    }

    submitForm(){
        fetch('http://127.0.0.1:8000/api/class/',{
            method:'POST',
            body:JSON.stringify(this.state),
            headers:{
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then(response=>response.json())
        .then((data)=>console.log(data));

        this.setState({
            name:'',
            number_of_students: '',
        });
    }

    handleModal(){
        this.setState({show: !this.state.show})
    }

    render() {
        return (
        <Card className="text-center" style={{ width: '18rem' }}>
            <Card.Img variant="top" src='./images/Add.png' />
            <Card.Body>
                <Button variant="outline-warning" onClick={()=>{this.handleModal()}}>Dodaj Klasę</Button>
                <Modal show={this.state.show} onHide={()=>this.handleModal()} animation={false}>
                <Modal.Header closeButton>
                <Modal.Title>Dodaj Klasę</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group >
                        <Form.Label>Nazwa: </Form.Label>
                        <Form.Control value={this.state.name} name="name" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Liczba Uczniów: </Form.Label>
                        <Form.Control value={this.state.number_of_students} name="number_of_students" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type="submit" onClick={this.submitForm}>
                        Dodaj
                    </Button>
                </Modal.Footer>
            </Modal>
            </Card.Body>
        </Card>
        )
    }
}

export default AddClassCardUI