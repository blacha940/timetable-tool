import React, { Component } from 'react';
import { Card, Button, Modal, Form } from 'react-bootstrap';

class AddClassroomCardUI extends Component {

    constructor(){
        super();
        this.state={
            id: '',
            name: '',
            capacity: '',
            description: '',
            building: '',
            show:false
        }
        this.changeHandler=this.changeHandler.bind(this);
        this.submitForm=this.submitForm.bind(this);
    }

    changeHandler(event){
        this.setState({
            [event.target.name]:event.target.value
        });
    }

    submitForm(){
        fetch('http://127.0.0.1:8000/api/classroom/',{
            method:'POST',
            body:JSON.stringify(this.state),
            headers:{
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then(response=>response.json())
        .then((data)=>console.log(data));

        this.setState({
            id: '',
            name: '',
            capacity: '',
            description: '',
            building: '',
        });
    }

    handleModal(){
        this.setState({show: !this.state.show})
    }

    render() {
        return (
        <Card className="text-center" style={{ width: '18rem' }}>
            <Card.Img variant="top" src='https://cdn3.iconfinder.com/data/icons/nature-37/120/tempaltaaaeaqas-512.png' />
            <Card.Body>
                <Button variant="outline-warning" onClick={()=>{this.handleModal()}}>Dodaj Salę</Button>
                <Modal show={this.state.show} onHide={()=>this.handleModal()} animation={false}>
                <Modal.Header closeButton>
                <Modal.Title>Dodaj Salę</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h1>{this.props.buildingID}</h1>
                    <Form.Group >
                        <Form.Label>Nazwa: </Form.Label>
                        <Form.Control value={this.state.name} name="name" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Liczba miejsc siedzących: </Form.Label>
                        <Form.Control value={this.state.capacity} name="capacity" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Opis: </Form.Label>
                        <Form.Control value={this.state.description} name="description" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>ID budynku: </Form.Label>
                        <Form.Control value={this.state.building} name="building" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type="submit" onClick={this.submitForm}>
                        Dodaj
                    </Button>
                </Modal.Footer>
            </Modal>
            </Card.Body>
        </Card>
        )
    }
}

export default AddClassroomCardUI