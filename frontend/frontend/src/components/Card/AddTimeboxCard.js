import React, { Component } from 'react';
import { Card, Button, Modal, Form } from 'react-bootstrap';

class AddTimeboxCardUI extends Component {

    constructor(){
        super();
        this.state={
            start:'',
            stop:'',
            show:false
        }
        this.changeHandler=this.changeHandler.bind(this);
        this.submitForm=this.submitForm.bind(this);
    }

    changeHandler(event){
        this.setState({
            [event.target.name]:event.target.value
        });
    }

    submitForm(){
        fetch('http://127.0.0.1:8000/api/timebox/',{
            method:'POST',
            body:JSON.stringify(this.state),
            headers:{
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then(response=>response.json())
        .then((data)=>console.log(data));

        this.setState({
            start:'',
            stop:'',
        });
    }

    handleModal(){
        this.setState({show: !this.state.show})
    }

    render() {
        return (
        <Card className="text-center" style={{ width: '18rem' }}>
            <Card.Img variant="top" src='./images/Add.png' />
            <Card.Body>
                <Button variant="outline-warning" onClick={()=>{this.handleModal()}}>Dodaj Godziny Lekcyjne</Button>
                <Modal show={this.state.show} onHide={()=>this.handleModal()} animation={false}>
                <Modal.Header closeButton>
                <Modal.Title>Dodaj Godziny Lekcujne</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group >
                        <Form.Label>Start: </Form.Label>
                        <Form.Control value={this.state.start} name="start" onChange={this.changeHandler} type="time"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Stop: </Form.Label>
                        <Form.Control value={this.state.stop} name="stop" onChange={this.changeHandler} type="time"/>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type="submit" onClick={this.submitForm}>
                        Dodaj
                    </Button>
                </Modal.Footer>
            </Modal>
            </Card.Body>
        </Card>
        )
    }
}

export default AddTimeboxCardUI