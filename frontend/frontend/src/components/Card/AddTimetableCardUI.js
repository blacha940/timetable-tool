// import React, { Component } from 'react';
// import { Card, Button, Modal, Form } from 'react-bootstrap';

// class AddTimetableCardUI extends Component {

//     constructor(){
//         super();
//         var today = new Date();
//         var dd = today.getDate();
//         var mm = today.getMonth()+1; 
//         var yyyy = today.getFullYear();
//         this.state={
//             id: '',
//             name: '',
//             creation_date: {yyyy}+'-'+{mm}+'-'+{dd},
//             date_of_acceptance: '',
//             is_accepted: false,
//             show:false
//         }
//         this.changeHandler=this.changeHandler.bind(this);
//         this.submitForm=this.submitForm.bind(this);
//     }

//     changeHandler(event){
//         this.setState({
//             [event.target.name]:event.target.value
//         });
//     }

//     submitForm(){
//         fetch('http://127.0.0.1:8000/api/timetable/',{
//             method:'POST',
//             body:JSON.stringify(this.state),
//             headers:{
//                 'Content-type': 'application/json; charset=UTF-8',
//             },
//         })
//         .then(response=>response.json())
//         .then((data)=>console.log(data));

//         this.setState({
//             id: '',
//             name: '',
//             creation_date: '',
//             date_of_acceptance: '',
//             is_accepted: false,
//         });
//     }

//     handleModal(){
//         this.setState({show: !this.state.show})
//     }

//     render() {
//         return (
//         <Card className="text-center" style={{ width: '18rem' }}>
//             <Card.Img variant="top" src='https://pics.wikifeet.com/Joanna-Halpin-Feet-5701905.jpg' />
//             <Card.Body>
//                 <Button variant="outline-warning" onClick={()=>{this.handleModal()}}>Dodaj Plan Zajęć</Button>
//                 <Modal show={this.state.show} onHide={()=>this.handleModal()} animation={false}>
//                 <Modal.Header closeButton>
//                 <Modal.Title>Dodaj Plan Zajęć</Modal.Title>
//                 </Modal.Header>
//                 <Modal.Body>
//                     <Form.Group >
//                         <Form.Label>Nazwa: </Form.Label>
//                         <Form.Control value={this.state.name} name="name" onChange={this.changeHandler} type="text"/>
//                     </Form.Group>
//                 </Modal.Body>
//                 <Modal.Footer>
//                     <Button variant="primary" type="submit" onClick={this.submitForm}>
//                         Dodaj
//                     </Button>
//                 </Modal.Footer>
//             </Modal>
//             </Card.Body>
//         </Card>
//         )
//     }
// }

// export default AddTimetableCardUI