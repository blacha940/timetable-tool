import React, { Component } from 'react';
import { Card, Button, Modal, Form } from 'react-bootstrap';
import {Link} from 'react-router-dom'

class BuildingCardUI extends Component {

    constructor(){
        super();
        this.state={
            id: "", 
            name: "",
            street: "",
            house_number: "",
            postal_code: "",
            city: "",
            show:false
        }
        this.changeHandler=this.changeHandler.bind(this);
        this.submitForm=this.submitForm.bind(this);
    }

    changeHandler(event){
        this.setState({
            [event.target.name]:event.target.value
        });
    }

    submitForm(){
        var id=this.props.object.id;
        fetch('http://127.0.0.1:8000/api/buildnigs/'+id+'/',{
            method:'PUT',
            body:JSON.stringify(this.state),
            headers:{
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then(response=>response.json())
        .then((data)=>console.log(data));

    }

    fetchData(){
        var id=this.props.object.id;
        fetch('http://127.0.0.1:8000/api/buildnigs/'+id)
        .then(response=>response.json())
        .then((data)=>{
            this.setState({
              id: data.id,
              name: data.name,
              street: data.street,
              house_number: data.house_number,
              postal_code: data.postal_code,
              city: data.city,
            });
            console.log(data)
        });
    }

    componentDidMount(){
        this.fetchData();
    }

    deleteData(id){
        console.log(id);
        fetch('http://127.0.0.1:8000/api/buildnigs/'+id+'/',{
            method:'DELETE',
            body:JSON.stringify(this.state), 
        })
        .then(response=>response)
    }


    handleModal(){
        this.setState({show: !this.state.show})
    }

  render() {
    return (
      <Card className="text-center" style={{ width: '18rem' }}>
        <Card.Img variant="top" src={this.props.img} />
        <Card.Body>
            <Link to={`/building/${this.props.object.id}`} className="btn outline-warning">{this.props.object.name}</Link>
            <Button variant="outline-warning" onClick={()=>{this.handleModal()}}>Szczegóły</Button>
                <Modal show={this.state.show} onHide={()=>this.handleModal()} animation={false}>
                <Modal.Header closeButton>
                <Modal.Title>Edytuj Budynek</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group >
                        <Form.Label>Nazwa: </Form.Label>
                        <Form.Control value={this.state.name} name="name" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Ulica: </Form.Label>
                        <Form.Control value={this.state.street} name="street" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                   <Form.Group >
                        <Form.Label>Numer: </Form.Label>
                        <Form.Control value={this.state.house_number} name="house_number" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Kod pocztowy: </Form.Label>
                        <Form.Control value={this.state.postal_code} name="postal_code" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Miasto: </Form.Label>
                        <Form.Control value={this.state.city} name="city" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type="submit" onClick={this.submitForm}>
                        Edytuj
                    </Button>
                    <Button variant="danger" type="submit" onClick={()=>this.deleteData(this.state.id)}>
                        Usuń
                    </Button>
                </Modal.Footer>
            </Modal>
        </Card.Body>
      </Card>
    )
  }
}

export default BuildingCardUI