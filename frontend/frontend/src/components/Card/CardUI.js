import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import {Link} from 'react-router-dom'

class CardUI extends Component {

  render() {
    return (
      <Card className="text-center" style={{ width: '18rem' }}>
        <Card.Img variant="top" src={this.props.img} />
        <Card.Body>
          <Link to={this.props.url} className="btn outline-warning">{this.props.name}</Link>
        </Card.Body>
      </Card>
    )
  }
}

export default CardUI