import React, { Component } from 'react';
import { Card, Button, Modal, Form } from 'react-bootstrap';

class ClassroomCardUI extends Component {

    constructor(){
        super();
        this.state={
            id: '',
            name: '',
            capacity: '',
            description: '',
            building: '',
            show:false
        }
        this.changeHandler=this.changeHandler.bind(this);
        this.submitForm=this.submitForm.bind(this);
    }

    changeHandler(event){
        this.setState({
            [event.target.name]:event.target.value
        });
    }

    submitForm(){
        var id=this.props.classroomObject.id;
        fetch('http://127.0.0.1:8000/api/classroom/'+id+'/',{
            method:'PUT',
            body:JSON.stringify(this.state),
            headers:{
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then(response=>response.json())
        .then((data)=>console.log(data));

    }

    fetchData(){
        var id=this.props.classroomObject.id;
        fetch('http://127.0.0.1:8000/api/classroom/'+id)
        .then(response=>response.json())
        .then((data)=>{
            this.setState({
                id: data.id,
                name: data.name,
                capacity: data.capacity,
                description: data.description,
                building: data.building,
            });
            console.log(data)
        });
    }

    componentDidMount(){
        this.fetchData();
    }

    deleteData(id){
        console.log(id);
        fetch('http://127.0.0.1:8000/api/classroom/'+id+'/',{
            method:'DELETE',
            body:JSON.stringify(this.state), 
        })
        .then(response=>response)
    }


    handleModal(){
        this.setState({show: !this.state.show})
    }

  render() {
    return (
      <Card className="text-center" style={{ width: '18rem' }}>
        <Card.Img variant="top" src={this.props.img} />
        <Card.Body>
            <Button variant="outline-warning" onClick={()=>{this.handleModal()}}>{this.props.classroomObject.name}</Button>
                <Modal show={this.state.show} onHide={()=>this.handleModal()} animation={false}>
                <Modal.Header closeButton>
                <Modal.Title>Edytuj Klasę</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group >
                        <Form.Label>Nazwa: </Form.Label>
                        <Form.Control value={this.state.name} name="name" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Liczba miejsc siedzących: </Form.Label>
                        <Form.Control value={this.state.capacity} name="capacity" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Opis: </Form.Label>
                        <Form.Control value={this.state.description} name="description" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>ID budynku: </Form.Label>
                        <Form.Control value={this.state.building} name="building" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type="submit" onClick={this.submitForm}>
                        Edytuj
                    </Button>
                    <Button variant="danger" type="submit" onClick={()=>this.deleteData(this.state.id)}>
                        Usuń
                    </Button>
                </Modal.Footer>
            </Modal>
        </Card.Body>
      </Card>
    )
  }
}

export default ClassroomCardUI