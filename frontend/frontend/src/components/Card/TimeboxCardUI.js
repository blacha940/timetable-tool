import React, { Component } from 'react';
import { Card, Button, Modal, Form } from 'react-bootstrap';

class TimeboxCardUI extends Component {

    constructor(){
        super();
        this.state={
            id: '',
            start:'',
            stop:'',
            show:false
        }
        this.changeHandler=this.changeHandler.bind(this);
        this.submitForm=this.submitForm.bind(this);
    }

    changeHandler(event){
        this.setState({
            [event.target.name]:event.target.value
        });
    }

    submitForm(){
        var id=this.props.object.id;
        fetch('http://127.0.0.1:8000/api/timebox/'+id+'/',{
            method:'PUT',
            body:JSON.stringify(this.state),
            headers:{
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then(response=>response.json())
        .then((data)=>console.log(data));

    }

    fetchData(){
        var id=this.props.object.id;
        fetch('http://127.0.0.1:8000/api/timebox/'+id)
        .then(response=>response.json())
        .then((data)=>{
            this.setState({
                id: data.id,
                start:data.start,
                stop:data.stop,
            });
            console.log(data)
        });
    }

    componentDidMount(){
        this.fetchData();
    }

    deleteData(id){
        console.log(id);
        fetch('http://127.0.0.1:8000/api/timebox/'+id+'/',{
            method:'DELETE',
            body:JSON.stringify(this.state), 
        })
        .then(response=>response)
    }


    handleModal(){
        this.setState({show: !this.state.show})
    }

  render() {
    return (
      <Card className="text-center" style={{ width: '18rem' }}>
        <Card.Img variant="top" src={this.props.img} />
        <Card.Body>
            <Button variant="outline-warning" onClick={()=>{this.handleModal()}}>{this.props.object.start} - {this.props.object.stop}</Button>
                <Modal show={this.state.show} onHide={()=>this.handleModal()} animation={false}>
                <Modal.Header closeButton>
                <Modal.Title>Edytuj Nauczyciela</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group >
                        <Form.Label>Początek: </Form.Label>
                        <Form.Control value={this.state.start} name="start" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Koniec: </Form.Label>
                        <Form.Control value={this.state.stop} name="stop" onChange={this.changeHandler} type="text"/>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type="submit" onClick={this.submitForm}>
                        Edytuj
                    </Button>
                    <Button variant="danger" type="submit" onClick={()=>this.deleteData(this.state.id)}>
                        Usuń
                    </Button>
                </Modal.Footer>
            </Modal>
        </Card.Body>
      </Card>
    )
  }
}

export default TimeboxCardUI