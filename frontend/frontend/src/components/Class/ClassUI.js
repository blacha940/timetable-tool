import React, { Component } from 'react';
import ClassCardUI from '../Card/ClassCardUI';
import AddClassCardUI from '../Card/AddClassCardUI';

class ClassUI extends Component {

    state = {
        classes: []
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/class/')
        .then((response) => response.json())
        .then(classList => {
            this.setState({ classes: classList });
        });
    }

    render() {
        return ( <div>
            {this.state.classes.map((school_class) => (
                <ClassCardUI key = {school_class.id} object = {school_class} img = './images/Class.png'/>
            ))}
            <AddClassCardUI/>
            </div>
        )
  }
}

export default ClassUI