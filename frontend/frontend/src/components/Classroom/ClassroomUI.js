import React, { Component } from 'react';
import ClassroomCardUI from '../Card/ClassroomCardUI';
import AddClassroomCardUI from '../Card/AddClassroomCardUI';
import { withRouter } from "react-router";

class ClassroomUI extends Component {
    
    state = {
        building: this.props.match.params.id,
        classrooms: []
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/classroom/')
        .then((response) => response.json())
        .then(classroomList => {
            classroomList = classroomList.filter(item => item.building === parseInt(this.props.match.params.id))
            this.setState({ classrooms: classroomList });
        });
    }

    render() {
        return (
            <div>
                {this.state.classrooms.map((classroom) => (
                    <ClassroomCardUI key = {classroom.id} classroomObject = {classroom} img = 'https://cdn3.iconfinder.com/data/icons/nature-37/120/tempaltaaaeaqas-512.png'/>
                ))}
                <AddClassroomCardUI buildingID = {this.state.building}/>
            </div>
        )
  }
}

export default withRouter(ClassroomUI)