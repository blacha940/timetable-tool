import React, {useState, useEffect} from 'react'
import axios from 'axios';

function FetchData() {

    const [teachers, setTeachers] = useState([])
    const [id, setId] = useState(1)

    useEffect (() => {
        axios.get(`http://127.0.0.1:8000/api/teachers/${id}`)
        .then(resp => {
            console.log(resp.data)
            setTeachers(resp.data)
        })
        .catch(error => console.log(error))
    }, [id])

    return (
        <>
            {/* {teachers.map(teacher => (
            <h3 key = {teacher.id}>{teacher.name}</h3>
            ))} */}
            <input type="text" value={id} onChange = {e => setId(e.target.value)}/>
            <h3>{teachers.name} {teachers.surname}</h3>
        </>
    )
}

export default FetchData