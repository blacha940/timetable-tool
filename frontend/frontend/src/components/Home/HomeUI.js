import React, { Component } from 'react';
import img1 from '../../images/Timetable Tool Logo.jpg';

class HomeUI extends Component {

  render() {
    return (
      <img src={img1} alt='Timetable Tool logo'></img>
    )
  }
}

export default HomeUI