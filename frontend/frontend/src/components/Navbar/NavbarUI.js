import React, { Component } from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import './Navbar.css'

class NavbarUI extends Component {

    render() {
        return (
            <>
                <Navbar bg="warning" variant="dark" className = 'navbar'>
                    <Navbar.Brand href="/">Timetable Tool</Navbar.Brand>
                    <Nav className="mr-auto">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href="/school">Szkoła</Nav.Link>
                    <Nav.Link href="/timetable">Plany Zajęć</Nav.Link>
                    <Nav.Link href="/create_time_table">Stwórz Plan Zajęć</Nav.Link>
                    </Nav>
                </Navbar>
            </>
        )
    }
}

export default NavbarUI