import React, { Component } from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import CardUI from '../Card/CardUI';

class SchoolUI extends Component {

  render() {
    return (
        <Container>
        <Row>
            <Col><CardUI name = 'Budynki' url = '/building' img = './images/Building.png'/></Col>
            <Col><CardUI name = 'Godziny Lekcyjne' url = '/timebox' img = './images/Timebox.png'/></Col>
            <Col><CardUI name = 'Nauczyciele' url = '/teacher' img = './images/Teacher.png'/></Col>
            <Col><CardUI name = 'Klasy' url = '/class' img = './images/Class.png'/></Col>
        </Row>
        </Container>
    )
  }
}

export default SchoolUI