import React, { Component } from 'react';
import TeacherCardUI from '../Card/TeacherCardUI';
import AddTeacherCardUI from '../Card/AddTeacherCardUI';

class TeacherUI extends Component {

    state = {
        teachers: []
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/teachers/')
        .then((response) => response.json())
        .then(teacherList => {
            this.setState({ teachers: teacherList });
        });
    }

    render() {
        return ( <div>
            {this.state.teachers.map((teacher) => (
                <TeacherCardUI key = {teacher.id} object = {teacher} img = './images/Teacher.png'/>
            ))}
            <AddTeacherCardUI/>
        </div>

        )
  }
}

export default TeacherUI