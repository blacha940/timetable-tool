import React, { Component } from 'react';
import TimeboxCardUI from '../Card/TimeboxCardUI';
import AddTimeboxCardUI from '../Card/AddTimeboxCard';

class TimeboxUI extends Component {

    state = {
        timeboxs: []
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/timebox/')
        .then((response) => response.json())
        .then(timeboxList => {
            this.setState({ timeboxs: timeboxList });
        });
    }

    render() {
        return ( <div>
            {this.state.timeboxs.map((timebox) => (
                <TimeboxCardUI key = {timebox.id} object = {timebox} img = './images/Timebox.png'/>
            ))}
            <AddTimeboxCardUI/>
        </div>

        )
  }
}

export default TimeboxUI