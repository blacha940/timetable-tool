import React, { Component } from 'react';
import TimetableCardUI from '../Card/TimetableCardUI';
// import AddTimetableCardUI from '../Card/AddTimetableCardUI';

class TimeboxUI extends Component {

    state = {
        timetables: []
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/timetable/')
        .then((response) => response.json())
        .then(timetableList => {
            this.setState({ timetables: timetableList });
        });
    }

    render() {
        return ( <div>
            {this.state.timetables.map((timetable) => (
                <TimetableCardUI key = {timetable.id} object = {timetable} img = './images/Schedule.png'/>
            ))}
            {/* <AddTimetableCardUI/> */}
        </div>

        )
  }
}

export default TimeboxUI