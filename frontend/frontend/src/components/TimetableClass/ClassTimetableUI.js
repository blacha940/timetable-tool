import React, { Component } from 'react';
import { Card} from 'react-bootstrap';

class ClassTimetableUI extends Component {

    state = {
        class_id: this.props.match.params.id2,
        classLessons: []
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/lesson/')
        .then((response) => response.json())
        .then(classLessonsList => {
            classLessonsList = classLessonsList.filter(item => item.class_id === parseInt(this.state.class_id))
            this.setState({ classLessons: classLessonsList });
        });
    }

  render() {
    return (
        <>
            {console.log(this.state.class_id)}
            {console.log(this.state.classLessons)}
            {this.state.classLessons.map(lesson => {
              return (
                <Card style={{ width: '18rem' }} key = {lesson.id}>
                  <Card.Body>
                    <Card.Title>Lesson ID: {lesson.id}</Card.Title>
                    <Card.Text>
                      Teacher ID: {lesson.teacher} <br/>
                      Subject ID: {lesson.subject} <br/>
                      Class ID: {lesson.class_id} <br/>
                      Timebox ID: {lesson.timebox} <br/>
                      Week Day ID: {lesson.week_day} <br/>
                      Classroom ID: {lesson.classroom} <br/>
                      Timetable ID: {lesson.timetable} <br/>
                    </Card.Text>
                  </Card.Body>
                </Card>
              )
            })}
        </>
    )
  }
}

export default ClassTimetableUI