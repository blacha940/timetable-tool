import React, { Component } from 'react';
import ClassCardUI2 from './ClassCardUI2'

class ClassUI2 extends Component {

    state = {
        classes: []
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/class/')
        .then((response) => response.json())
        .then(classObjectList => {
            this.setState({ classes: classObjectList });
        });
    }

    render() {
        console.log(this.props.match.params.id)
        return ( <div>
            {this.state.classes.map((classObject) => (
                <ClassCardUI2 key = {classObject.id} object = {classObject} id={this.props.match.params.id} img = 'https://cdn3.iconfinder.com/data/icons/nature-37/120/tempaltaaaeaqas-512.png'/>
            ))}
        </div>

        )
  }
}

export default ClassUI2