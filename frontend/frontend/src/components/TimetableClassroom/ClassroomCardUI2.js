import React, { Component } from 'react';
import { Card} from 'react-bootstrap';
import {Link} from 'react-router-dom'

class ClassroomCardUI2 extends Component {

  render() {
    return (
      <Card className="text-center" style={{ width: '18rem' }}>
        <Card.Img variant="top" src={this.props.img} />
        <Card.Body>
            <Link to={`/timetable/${this.props.id}/classroom/${this.props.object.id}`} className="btn outline-warning">{this.props.object.name}</Link>
        </Card.Body>
      </Card>
    )
  }
}

export default ClassroomCardUI2