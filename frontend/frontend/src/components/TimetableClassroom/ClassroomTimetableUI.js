import React, { Component } from 'react';
import { Card} from 'react-bootstrap';

class ClassroomTimetableUI extends Component {

    state = {
        class_id: this.props.match.params.id2,
        classroom_lessons: []
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/lesson/')
        .then((response) => response.json())
        .then(classroomLessonList => {
            classroomLessonList = classroomLessonList.filter(item => item.classroom === parseInt(this.state.class_id))
            this.setState({ classroom_lessons: classroomLessonList });
        });
    }

  render() {
    console.log(this.state.class_id)
    console.log(this.state.classroom_lessons)
    return (
        <>
            {this.state.classroom_lessons.map(lesson => {
              return (
                <Card style={{ width: '18rem' }} key = {lesson.id}>
                  <Card.Body>
                    <Card.Title>Lesson ID: {lesson.id}</Card.Title>
                    <Card.Text>
                      Teacher ID: {lesson.teacher} <br/>
                      Subject ID: {lesson.subject} <br/>
                      Class ID: {lesson.class_id} <br/>
                      Timebox ID: {lesson.timebox} <br/>
                      Week Day ID: {lesson.week_day} <br/>
                      Classroom ID: {lesson.classroom} <br/>
                      Timetable ID: {lesson.timetable} <br/>
                    </Card.Text>
                  </Card.Body>
                </Card>
              )
            })}
        </>
    )
  }
}

export default ClassroomTimetableUI