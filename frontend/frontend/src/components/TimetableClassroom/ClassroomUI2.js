import React, { Component } from 'react';
import ClassroomCardUI2 from './ClassroomCardUI2'

class ClassroomUI2 extends Component {

    state = {
        classrooms: []
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/classroom/')
        .then((response) => response.json())
        .then(classroomList => {
            this.setState({ classrooms: classroomList });
        });
    }

    render() {
        console.log(this.props.match.params.id)
        return ( <div>
            {this.state.classrooms.map((classroom) => (
                <ClassroomCardUI2 key = {classroom.id} object = {classroom} id={this.props.match.params.id} img = 'https://cdn3.iconfinder.com/data/icons/nature-37/120/tempaltaaaeaqas-512.png'/>
            ))}
        </div>

        )
  }
}

export default ClassroomUI2