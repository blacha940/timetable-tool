import React, { Component } from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import CardUI from '../Card/CardUI';

class TimetableMenuUI extends Component {

  render() {
    return (
        <Container>
        <Row>
            <Col><CardUI name = 'Sale' url = {`/timetable/${this.props.match.params.id}/classroom`} img = 'https://cdn3.iconfinder.com/data/icons/nature-37/120/tempaltaaaeaqas-512.png'/></Col>
            <Col><CardUI name = 'Nauczyciele' url = {`/timetable/${this.props.match.params.id}/teacher`} img = 'https://cdn3.iconfinder.com/data/icons/nature-37/120/tempaltaaaeaqas-512.png'/></Col>
            <Col><CardUI name = 'Klasy' url = {`/timetable/${this.props.match.params.id}/class`} img = 'https://cdn3.iconfinder.com/data/icons/nature-37/120/tempaltaaaeaqas-512.png'/></Col>
        </Row>
        </Container>
    )
  }
}

export default TimetableMenuUI