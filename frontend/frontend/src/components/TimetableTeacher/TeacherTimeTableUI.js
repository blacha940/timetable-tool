import React, { Component } from 'react';
import { Card} from 'react-bootstrap';

class TeacherTimeTableUI extends Component {

    state = {
        teacher_id: this.props.match.params.id2,
        teacher_lessons: []
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/lesson/')
        .then((response) => response.json())
        .then(teacherLessonList => {
            teacherLessonList = teacherLessonList.filter(item => item.teacher === parseInt(this.state.teacher_id))
            this.setState({ teacher_lessons: teacherLessonList });
        });
    }

  render() {
    return (
        <>
            {this.state.teacher_lessons.map(lesson => {
              return (
                <Card style={{ width: '18rem' }} key = {lesson.id}>
                  <Card.Body>
                    <Card.Title>Lesson ID: {lesson.id}</Card.Title>
                    <Card.Text>
                      Teacher ID: {lesson.teacher} <br/>
                      Subject ID: {lesson.subject} <br/>
                      Class ID: {lesson.class_id} <br/>
                      Timebox ID: {lesson.timebox} <br/>
                      Week Day ID: {lesson.week_day} <br/>
                      Classroom ID: {lesson.classroom} <br/>
                      Timetable ID: {lesson.timetable} <br/>
                    </Card.Text>
                  </Card.Body>
                </Card>
              )
            })}
        </>
    )
  }
}

export default TeacherTimeTableUI