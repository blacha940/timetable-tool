import React, { Component } from 'react';
import TeacherCardUI2 from './TeacherCardUI2'

class TeacherUI2 extends Component {

    state = {
        teachers: []
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/teachers/')
        .then((response) => response.json())
        .then(teacherList => {
            this.setState({ teachers: teacherList });
        });
    }

    render() {
        console.log(this.props.match.params.id)
        return ( <div>
            {this.state.teachers.map((teacher) => (
                <TeacherCardUI2 key = {teacher.id} object = {teacher} id={this.props.match.params.id} img = 'https://cdn3.iconfinder.com/data/icons/nature-37/120/tempaltaaaeaqas-512.png'/>
            ))}
        </div>

        )
  }
}

export default TeacherUI2