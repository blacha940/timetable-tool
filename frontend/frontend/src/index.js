import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import BuildingUI from './components/Building/BuildingUI';
import ClassUI from './components/Class/ClassUI';
import CreateTimetableUI from './components/CreateTimetable/CreateTimetableUI';
import NavbarUI from './components/Navbar/NavbarUI';
import SchoolUI from './components/School/SchoolUI';
import TimetableUI from './components/Timetable/TimetableUI';
import TeacherUI from './components/Teacher/TeacherUI';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Route, BrowserRouter} from 'react-router-dom';
import ClassroomUI from './components/Classroom/ClassroomUI';
import TimeboxUI from './components/Timebox/TimeboxUI';
import HomeUI from './components/Home/HomeUI';
import TimetableMenuUI from './components/TimetableMenu/TimetableMenuUI.js'
import ClassTimetableUI from './components/TimetableClass/ClassTimetableUI.js'
// import ClassroomCardUI2 from './components/TimetableClassroom/ClassroomCardUI2.js'
import ClassroomUI2 from './components/TimetableClassroom/ClassroomUI2.js'
import TeacherUI2 from './components/TimetableTeacher/TeacherUI2.js'
import TeacherTimeTableUI from './components/TimetableTeacher/TeacherTimeTableUI';
import ClassroomTimetableUI from './components/TimetableClassroom/ClassroomTimetableUI';
import ClassUI2 from './components/TimetableClass/ClassUI2';

function Router() {
  return (
    <BrowserRouter>
      <Route exact path = '/' component = {HomeUI}/>
      <Route exact path = '/building' component = {BuildingUI}/>
      <Route exact path = '/building/:id' component = {ClassroomUI}/>
      <Route exact path = '/class' component = {ClassUI}/>
      <Route exact path = '/create_time_table' component = {CreateTimetableUI}/>
      <Route exact path = '/timebox' component = {TimeboxUI}/>
      <Route exact path = '/school' component = {SchoolUI}/>
      <Route exact path = '/school/building' component = {BuildingUI}/>
      <Route exact path = '/teacher' component = {TeacherUI}/>
      <Route exact path = '/timetable' component = {TimetableUI}/>
      <Route exact path = '/timetable/:id/classroom/:id2' component = {ClassroomTimetableUI}/>
      <Route exact path = '/timetable/:id/classroom' component = {ClassroomUI2}/>
      <Route exact path = '/timetable/:id/class/:id2' component = {ClassTimetableUI}/>
      <Route exact path = '/timetable/:id/class' component = {ClassUI2}/>
      <Route exact path = '/timetable/:id/teacher/:id2' component = {TeacherTimeTableUI}/>
      <Route exact path = '/timetable/:id/teacher' component = {TeacherUI2}/>
      <Route exact path = '/timetable/:id' component = {TimetableMenuUI}/>


    </BrowserRouter>
  )
}

ReactDOM.render(
  <React.StrictMode>
    <NavbarUI/>
    <Router/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
